import React, { Component } from 'react'
import axios from 'axios'

class GetListOfNotes extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             notes : [],
             errorMsg : ''
        }
    }

    componentDidMount(){
        axios.get("http://localhost:8080/getAllNotes")
        .then( response => {
            console.log(response)
            this.setState({notes : response.data})
        }
        )
        .catch( error => {
            console.log(error)
            this.setState({errorMsg : 'Error retriving data'})
        }
            )
    }
    
    render() {
        const {notes, errorMsg} = this.state
        return (
            <div>
                List of Notes 
                {
                    notes.length ?
                    notes.map(notes => <div key={notes.id}>{notes.title}</div>) :
                    null
                }
            </div>
        )
    }
}

export default GetListOfNotes
